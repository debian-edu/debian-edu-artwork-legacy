# Debian Edu artwork legacy themes

## Introduction and Contact

Collection of images, themes, wallpapers and splash screens for use with
the Debian Edu distribution from ancient times worth to be kept in
Debian.

Contact debian-edu@lists.debian.org if you have comments and suggestions.

## Themes we liked...

### Debian Edu Spacefun

The Debian Edu Spacefun theme was the official Debian Edu for Debian Edu
6 (aka squeze). It has been removed from the official Debian Edu Artwork
package and will be reintroduce as a separate package as it has been used
for years on systems run by the IT-Zukunft Schule project in Nothern
Germany.

## License

This package is licensed using the GNU General Public License v2 or
later.  Only images with GPL compatible license should be added to this
package.  For images, the original file format (used when drawing the
image) should be included as well.
